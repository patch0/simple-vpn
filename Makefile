#!/usr/bin/make

HOSTNAME := $(shell hostname -f)

all: server.conf tls.key dh.pem ca/ca.crt ca/ca.crl servers/${HOSTNAME}/${HOSTNAME}.crt

%.key:
	@echo "\n**** Generating private key $@ ****"
	@mkdir -p $(@D)
	@certtool --hash SHA256 --generate-privkey --outfile $@

%.req: %.key %.tmpl
	@echo "\n**** Generating cert request $@ with key $< ****"
	@mkdir -p $(@D)
	@certtool --hash SHA256 --generate-request --template $(lastword $*).tmpl --load-privkey $< --outfile $@

%.crt: %.req ca/ca.crt ca/ca.key
	@echo "\n**** Signing $< to generate $@ ****"
	@certtool --hash SHA256 --generate-certificate  --template $(basename $@).tmpl --load-ca-privkey ca/ca.key --load-ca-certificate ca/ca.crt --load-request $< --outfile $@

%.crl: %.revoked %.key %.crt
	@echo "\n**** Generating CRL $@ ****"
	@if [ -s $< ] ; then \
	  certtool --hash SHA256 --generate-crl --load-ca-privkey $(basename $@).key --template templates/crl.tmpl --load-ca-certificate $(basename $@).crt --load-certificate $(basename $@).revoked --outfile $@ ; \
	else \
	  certtool --hash SHA256 --generate-crl --load-ca-privkey $(basename $@).key --template templates/crl.tmpl --load-ca-certificate $(basename $@).crt --outfile $@ ; \
	fi

%.ovpn: templates/ovpn.tmpl %.crt %.key ca/ca.crt tls.key
	@sed -e 's/HOSTNAME/${HOSTNAME}/g' $< > $@
	@chmod 600 $@
	@echo "<ca>\n$$(cat ca/ca.crt)\n</ca>" >> $@
	@echo "<cert>\n$$(cat $(basename $@).crt)\n</cert>" >>  $@
	@echo "<key>\n$$(sed '/-----BEGIN RSA PRIVATE KEY-----/,$$!d' $(basename $@).key)\n</key>" >>  $@
	@echo "<tls-crypt>\n$$(cat tls.key)\n</tls-crypt>" >> $@
	@echo '*** You can now pass `$@` to the user ***'

## Server tasks #############################################################
server.conf: templates/server.conf.tmpl
	@echo "\n*** Generating OpenVPN config $@ ***"
	@sed -e 's/HOSTNAME/${HOSTNAME}/g' $< > $@

tls.key:
	@echo "\n*** Generating OpenVPN secret key $@***"
	@/usr/sbin/openvpn --genkey --secret $@

dh.pem:
	@echo "\n*** Generating Diffie-Helman params: $@***"
	@certtool --generate-dh-params > $@

## CA tasks #################################################################
ca/ca.crt: ca/ca.key ca/ca.tmpl
	@echo "\n**** Generating CA certificate $@ ****"
	@mkdir -p $(@D)
	@sed -e 's/HOSTNAME/${HOSTNAME}/g' templates/ca.tmpl > $@
	@certtool --hash SHA256 --generate-self-signed --template ca/ca.tmpl --load-privkey $< --outfile $@

ca/ca.revoked: ca/revoked
	@echo "\n**** Generating $@ ****"
	@cat $(@D)/revoked/*/*.crt > $@ 2>/dev/null || touch $@

ca/revoked:
	@mkdir $@

## Per client tasks #########################################################
clients/%: clients/%/%.tmpl

clients/%.tmpl: templates/client.tmpl
	@echo dn= "cn=$(basename $(@F))" > $@
	@cat $< >> $@

servers/%.tmpl: templates/server.tmpl
	@echo dn= "cn=$(basename $(@F))" > $@
	@cat $< >> $@

ca/%.tmpl: templates/ca.tmpl
	@sed -e 's/HOSTNAME/${HOSTNAME}/g' $< > $@
	@cat $< >> $@

.PRECIOUS: %.key %.crt %.req %.ovpn %.conf

