# An easy OpenVPN implementation using gnutls

TODO: Write more here!

This setup is largely controlled through a Makefile.  To create a new client OpenVPN configuration, run

```
make clients/username-$(date +%Y%m%d)/username.ovpn
```

and that will generate lots of stuff.  The certificates last for a year.

Then you can activate your new server by running

```
systemctl enable openvpn@server
systemctl start openvpn@server
```

Then the user can start their OpenVPN tunnel by running

```
openvpn username.ovpn
```

They *must* have a shell account on the box, as we use PAM to authenticate.


